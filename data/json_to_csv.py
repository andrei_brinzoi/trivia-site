import json

csv_path = 'questions.csv.out'
json_path = 'questions.json'

letter_to_number = {
    'A': 0,
    'B': 1,
    'C': 2,
    'D': 3,
}

if __name__ == '__main__':
    with open(json_path, 'r') as f:
        json_data = json.load(f)

    print(len(json_data))

    with open(csv_path, 'w') as f:
        for idx, item in enumerate(json_data):
            s = f"{idx},{item['question']},{item['A']},{item['B']},{item['C']},{item['D']},{letter_to_number[item['answer']]}"
            f.write(s)
            f.write('\n')
