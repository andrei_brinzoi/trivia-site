from django.contrib import admin
from django.urls import path, include
from questions import views

urlpatterns = [
    path('', views.home, name='home'),
    path('admin/', admin.site.urls),
    path('questions/', include('questions.urls')),
]