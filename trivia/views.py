from django.http import HttpResponse


def home(request):
    # remove these print statements later
    print('\n\nRequest object:', request)
    print('Request object type:', type(request), '\n\n')

    html_tags = '''
    <h1>Test your knowledge with us!</h1>
    <h3>Are you ready?</h3>
        <button type="button" class="btn btn-primary">Primary</button>
    '''

    response = HttpResponse(html_tags)
    # remove these print statements later
    print('Response object:', response)
    print('Response object type:', type(response))
    print('\n\nUser-agent info :', end='')
    print(request.META['HTTP_USER_AGENT'], '\n\n')

    return response


def register_request(request):
    # remove these print statements later
    print('\n\nRequest object:', request)
    print('Request object type:', type(request), '\n\n')

    html_tags = '''
    <h1>Test your knowledge with us!</h1>
    <h3>Are you ready?</h3>
    <p>Categories for you:</p>
    <ul>
      <li>Geography</li>
      <li>History</li>
      <li>Science</li>
    </ul>'''

    response = HttpResponse(html_tags)
    # remove these print statements later
    print('Response object:', response)
    print('Response object type:', type(response))
    print('\n\nUser-agent info :', end='')
    print(request.META['HTTP_USER_AGENT'], '\n\n')

    return response
