from django.urls import path

from . import views

app_name = 'questions'

urlpatterns = [
    path('', views.home, name='home'),
    path('register/', views.register, name='register'),
    path('highscore/', views.highscore, name='high_score'),
    path('login/', views.login_request, name="login"),
    path("logout/", views.logout_request, name="logout"),
    path("take_quiz/", views.take_quiz, name="take_quiz"),
    path("validate_quiz/", views.validate_quiz, name="validate_quiz"),
]
