from django.db import models


class ResModel(models.Model):
    correct = models.IntegerField()
    incorrect = models.IntegerField()
    total_answered = models.IntegerField()
    total_questions = models.IntegerField()
    score = models.FloatField()
    user = models.CharField(max_length=30, default=None)
    def __str__(self):
        return 'result'