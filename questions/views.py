import csv
import random

from django.contrib import messages
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.forms import AuthenticationForm
from django.shortcuts import redirect
from django.shortcuts import render
from .models import ResModel

from .forms import NewUserForm


def get_questions():
    with open('data/questions.csv', mode='r', encoding="utf8") as csv_file:
        csv_reader = csv.DictReader(csv_file)
        return list(csv_reader)


QUESTIONS = get_questions()
QUIZ_LENGTH = 10


def home(request):
    return render(request, 'home.html')


def highscore(request):
    results = ResModel.objects.order_by('score').reverse()
    return render(request, 'questions/highscores.html', {'results': results})


def take_quiz(request):
    random_questions = random.choices(QUESTIONS, k=QUIZ_LENGTH)
    return render(request, 'questions/quiz.html', {'questions': random_questions})


def validate_quiz(request):
    print(request.POST)

    correct_questions = 0
    incorrect_questions = 0
    total_questions = 0

    for question_id, selected_result in request.POST.items():
        try:
            index = int(question_id)
        except ValueError:
            continue
        else:
            total_questions += 1

            if QUESTIONS[index]['correct_answer'] == selected_result:
                correct_questions += 1
            else:
                incorrect_questions += 1

    score = round(float(correct_questions) / QUIZ_LENGTH * 100, 2)

    result = {
            'correct': correct_questions,
            'incorrect': incorrect_questions,
            'total_answered': total_questions,
            'total_questions': QUIZ_LENGTH,
            'score': score,
            'user': request.user
        }
    ResModel(**result).save()
    return render(request, 'questions/result.html', {
        'result': result
    })


def register(request):
    message, error = None, None
    if request.method == "POST":
        form = NewUserForm(request.POST)
        if form.is_valid():
            form.save()
            register(request)
            message = "Registration successful."
            messages.success(request, message)
            return redirect("/questions/login/")
        error = "Unsuccessful registration. Invalid information."
        messages.error(request, error)
        return render(request=request, template_name="questions/register.html", context={
            "register_form": form,
            "error": error,
            "message": message
        })
    else:
        form = NewUserForm()
        return render(request=request, template_name="questions/register.html", context={"register_form": form})


def login_request(request):
    if request.method == "POST":
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                messages.info(request, f"You are now logged in as {username}.")
                return redirect("home")
            else:
                messages.error(request, "Invalid username or password.")
        else:
            error = "Invalid username or password."
            messages.error(request, error)
            return render(request=request, template_name="questions/login.html", context={
                "login_form": AuthenticationForm(), 'error': error
            })

    form = AuthenticationForm()
    return render(request=request, template_name="questions/login.html", context={"login_form": form})


def logout_request(request):
    logout(request)
    messages.info(request, "You have successfully logged out.")
    return redirect("home")
